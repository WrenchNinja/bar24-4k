function launch(){
  chrome.app.window.create(
  	'index.html', 
  	{
	  	id: 'main',
	    'innerBounds': {
	      'width': 1080,
	      'height': 1920
	    },
	    //resizable: false
	});
}

chrome.app.runtime.onLaunched.addListener(launch);