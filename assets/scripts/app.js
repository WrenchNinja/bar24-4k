/*
	TODO:
	- States inladen
	- 
*/

$(document).ready(function(){

	Firebase.INTERNAL.forceWebSockets();
	var databaseRef = new Firebase('https://fi-vip.firebaseio.com/');
	var updateUI;

	databaseRef.on('value', function(dataSnapshot){
		updateUI(dataSnapshot.val().status, dataSnapshot.val().screen.step, dataSnapshot.val().user.firstName, dataSnapshot.val().mood, dataSnapshot.val().drinks, dataSnapshot.val().drinkInfo.name, dataSnapshot.val().drinkInfo.drinkID, dataSnapshot.val().drinkInfo.description);
	});

	var statusCache;
	var stepCache;
	var moodCache;
	var messageTimeout;
	var currentHour;

	var determineTime = function(){
		currentHour = new Date().getHours();

		if(currentHour<6){
		    $("section.intro h1").html("Good night");
		}else if(currentHour<12 && currentHour > 5 ){
		    $("section.intro h1").html("Good morning");
		}else if(currentHour<18 && currentHour > 11){
		    $("section.intro h1").html("Good afternoon");
		} else {
			$("section.intro h1").html("Good evening");
		}
	} 
	determineTime();
	setInterval(function(){
		determineTime();
	}, 2000);
	

	var updateMessage = function(message){

		clearTimeout(messageTimeout);

		$("footer .message p span, footer .message p").addClass('changing');

		messageTimeout = setTimeout(function(){
			$("footer .message p span").html(message);
			$("footer .message p span, footer .message p").removeClass('changing');
		}, 1000);

	}

	var updateUI = function(status, step, user, mood, drinks, drinkName, drinkID, drinkDescription){
		// compare caches to realtimes, only update on difference

		// update mood if needed

		if(status == 0){

			$("header, footer, section, .mood, body").removeClass('active');
			//$("body").stop().animate({"background-position-x":"-8.75%", "background-position-y":"19%"}, 2500, 'swing');
			statusCache = 0;

		} else if(status == 1){
			var selectors = "header, footer, body";
			selectors = selectors.toString();
			$(selectors).addClass('active');

			var currentMood;
			if(mood != ""){
				currentMood = "." + mood;
			}
			$(currentMood).addClass('active');

			switch(step) {
			    case "Welcome":
			    	
			    	if(!stepCache){
			    		$("section.intro").addClass('active')
			    	} else {
			    		$("section.dranken, section.mood").addClass('exit-right').removeClass('active');
			    		$("section.intro").addClass('position-left');
			    		setTimeout(function(){
			    			$("section.intro").removeClass('position-left exit-left').addClass('active');
			    		},0);
			    	}

			    	$("section.dranken .categories").removeClass('active');

			    	if( stepCache != "Welcome"){
			    		updateMessage("Please tell us your name");
			    	}
			    	
			    	stepCache = "Welcome";
			    	
			        break;
			    case "Mood":

			    	if( stepCache == "Welcome" ){
			    		$("section.intro").addClass('exit-left').removeClass('active');
			    		$("section.mood").addClass('position-right');
			    		setTimeout(function(){
			    			$("section.mood").removeClass('position-right exit-left exit-right').addClass('active');
			    		},0);
			    	} else if( stepCache == "drinkSelection" || stepCache == "drinkDetail" || stepCache == "ordered" ){
			    		$("section.dranken").addClass('exit-right').removeClass('active');
			    		$("section.mood").addClass('position-right');
			    		setTimeout(function(){
			    			$("section.mood").removeClass('position-right exit-left exit-right').addClass('active');
			    		},0);
			    	}
			    	

			    	if( stepCache != "Mood"){
			    		updateMessage("Please select your mood");
			    	}

			    	$("section.dranken .categories").removeClass('active');

			    	$("section").removeClass('active');
			    	$("section.mood").addClass('active');
			    	$("section.mood h1 span").html(" " + user);

			    	if( mood == "" ){

			    		$(".moods .mood").removeClass('active');

			    	} else{

			    		$(".moods .mood").removeClass('active');
			    		$(".moods ."+ mood).addClass('active');

			    	}

			    	stepCache = "Mood";
			    	moodCache = mood;

			        break;
			    case "drinkSelection":

			    	if( stepCache == "drinkDetail" || stepCache == "ordered" ){
			    		$("section.dranken").removeClass('active');
			    		setTimeout(function(){
			    			$("section.dranken h1 span").html("drink");
			    			$("section.dranken").addClass('active');
			    		}, 1000);
			    		setTimeout(function(){
			    			$("section.dranken .categories").addClass('active');
			    		}, 2000);
			    	} else {
			    		$("section.dranken h1 span").html("drink");
			    		$("section.dranken .categories").removeClass('active');
			    		$("section.intro, section.mood").addClass('exit-left').removeClass('active');
			    		$("section.dranken").addClass('position-right');
			    		setTimeout(function(){
			    			$("section.dranken").removeClass('position-right exit-right').addClass('active');
			    		},0);

			    		setTimeout(function(){
			    			$("section.dranken .categories").addClass('active');
			    		}, 1250);
			    	}

			    	$("section.dranken .categories .categorie, section.dranken .currentCategorie div, section.dranken .drinks-loader").removeClass('active');
			    	$("section.dranken .drinks-loader .drink").removeClass('hide active');
			    	$("section.dranken .drink-info").removeClass('active');

			    	if( stepCache != "drinkSelection"){
			    		updateMessage("Choose what kind of beverage you want");
			    	}

			    	stepCache = "drinkSelection";

			    	break;
			    case "drinkDetail":

					//contentDiv.html("You selected " + drinks + ". What drink would you like to order?");
					
					if( stepCache == "drinkSelection" || stepCache == "ordered" ){
						$("section.dranken").removeClass('active');
						setTimeout(function(){
							$("section.dranken .currentCategorie").addClass('active');
							$("section.dranken h1 span").html(drinks);
							$("section.dranken").addClass('active');
						}, 1000);
					} else {
						$("section.dranken h1 span").html(drinks);
						$("section.dranken .categories").removeClass('active');
						$("section.intro, section.mood").addClass('exit-left').removeClass('active');
						$("section.dranken").addClass('position-right');
						setTimeout(function(){
							$("section.dranken").removeClass('position-right exit-right').addClass('active');
						},0);
					}

					$("section.dranken .categories, section.dranken .drinks-loader").removeClass('active');
					$("section.dranken .drink-info").removeClass('active ordered');

					switch(drinks){
						case "coffee or tea":
							$("section.dranken .categories .categorie, section.dranken .currentCategorie div, section.dranken .currentCategorie").removeClass('active');
							$("section.dranken .categories .coffee, section.dranken .currentCategorie .coffee, section.dranken .currentCategorie").addClass('active');
							break;
						case "soft drink":
							$("section.dranken .categories .categorie, section.dranken .currentCategorie div, section.dranken .currentCategorie").removeClass('active');
							$("section.dranken .categories .soft, section.dranken .currentCategorie .soft, section.dranken .currentCategorie").addClass('active');
							break;
						case "wine":
							$("section.dranken .categories .categorie, section.dranken .currentCategorie div, section.dranken .currentCategorie").removeClass('active');
							$("section.dranken .categories .wine, section.dranken .currentCategorie .wine, section.dranken .currentCategorie").addClass('active');
							break;
						case "beer":
							$("section.dranken .categories .categorie, section.dranken .currentCategorie div, section.dranken .currentCategorie").removeClass('active');
							$("section.dranken .categories .beer, section.dranken .currentCategorie .beer, section.dranken .currentCategorie").addClass('active');
							break;
						case "gin & tonic":
							$("section.dranken .categories .categorie, section.dranken .currentCategorie div, section.dranken .currentCategorie").removeClass('active');
							$("section.dranken .categories .gin, section.dranken .currentCategorie .gin, section.dranken .currentCategorie").addClass('active');
							break;
					}

					if( stepCache != "drinkDetail" ){
						$("section.dranken .drinks-loader").html($("section.dranken .categories .active .drinks").html());
					}

					setTimeout(function(){
						$("section.dranken .drinks-loader").addClass('active');
					},1000);

					stepCache = "drinkDetail";

			    	if( drinkName != "" ){

			    		$("section.dranken .drinks-loader").addClass('description');
			    		$("section.dranken .drinks-loader .drink").addClass('hide').removeClass('active');
			    		$("section.dranken .drinks-loader #"+drinkID).removeClass('hide').addClass('active');
			    		$("section.dranken .currentCategorie").removeClass('active');

			    		$(".drink-info h2 span").html(drinkName);
			    		$(".drink-info p").html(drinkDescription);
			    		$("section.dranken .drink-info").addClass('active');

			    		updateMessage("You can order this beverage");

			    	} else {

			    		$("section.dranken .drinks-loader .drink").removeClass('hide active');
			    		$("section.dranken .drinks-loader").removeClass('description');
			    		$("section.dranken .drink-info").removeClass('active');

			    		updateMessage("Choose your beverage");

			    	}
			    	break;
			    case "ordered":

			    	updateMessage("Go back to order another beverage");

			    	if( stepCache != "drinkDetail" ){

			    		switch(drinks){
			    			case "coffee or tea":
			    				$("section.dranken .categories .categorie, section.dranken .currentCategorie div, section.dranken .currentCategorie").removeClass('active');
			    				$("section.dranken .categories .coffee, section.dranken .currentCategorie .coffee, section.dranken .currentCategorie").addClass('active');
			    				break;
			    			case "soft drink":
			    				$("section.dranken .categories .categorie, section.dranken .currentCategorie div, section.dranken .currentCategorie").removeClass('active');
			    				$("section.dranken .categories .soft, section.dranken .currentCategorie .soft, section.dranken .currentCategorie").addClass('active');
			    				break;
			    			case "wine":
			    				$("section.dranken .categories .categorie, section.dranken .currentCategorie div, section.dranken .currentCategorie").removeClass('active');
			    				$("section.dranken .categories .wine, section.dranken .currentCategorie .wine, section.dranken .currentCategorie").addClass('active');
			    				break;
			    			case "beer":
			    				$("section.dranken .categories .categorie, section.dranken .currentCategorie div, section.dranken .currentCategorie").removeClass('active');
			    				$("section.dranken .categories .beer, section.dranken .currentCategorie .beer, section.dranken .currentCategorie").addClass('active');
			    				break;
			    			case "gin & tonic":
			    				$("section.dranken .categories .categorie, section.dranken .currentCategorie div, section.dranken .currentCategorie").removeClass('active');
			    				$("section.dranken .categories .gin, section.dranken .currentCategorie .gin, section.dranken .currentCategorie").addClass('active');
			    				break;
			    		}

    			    	$("section.dranken .drinks-loader").html($("section.dranken .categories .active .drinks").html());
    		    		$("section.dranken .drinks-loader").addClass('description');
    		    		$("section.dranken .drinks-loader .drink").addClass('hide').removeClass('active');
    		    		$("section.dranken .drinks-loader #"+drinkID).removeClass('hide').addClass('active');
    		    		$("section.dranken .currentCategorie").removeClass('active');
			    	}

			    	$("section.dranken").addClass('active');
			    	$("section.dranken .drink-info").addClass('ordered');

	
			    	//contentDiv.html("one " + drinkName + " coming right up!");

			    	stepCache = "ordered";

			}

			statusCache = 1;

		}
	}

	var backgroundMove = function(){
		$("body").css({'background-position':"0% 0%"});
		$("body").stop().animate({"background-position-x":"-8.75%", "background-position-y":"19%"}, 5000, 'linear',
	  		function(){
	    		$("body").css({'background-position':"0% 0%"});
	    		if( $("body").hasClass('active') ){
	    			backgroundMove();
	    		} else {
	    			//$("body").stop().animate({"background-position-x":"-8.75%", "background-position-y":"19%"}, 2500, 'swing');
	    		}
	    		
	  		}
		);
	} 


	//backgroundMove();


	$(document).on("keypress", function(e){

		if(e.keyCode == 49){
			databaseRef.child('status').set(0);
		} else if(e.keyCode == 50){
			databaseRef.child('status').set(1);
		}

	});

});